#!/bin/sh
# Script for ubuntu on Azure, install docker package and an elastic search container from elastic.io
# 2020 Alberto Munoz <albertomunozv@gmail.com>  

# On a new vm, initialize the package index
sudo apt-get -y -q=2 update
# initialize some variables
# set the image to install
IMAGE="docker.elastic.co/elasticsearch/elasticsearch:7.6.2"
# sysctl optimization  
DOCKFILE=/etc/sysctl.d/10-docker.conf
# ---------------------------

# check if docker package is installed
# TODO: trap the binary file in a var to be eval
which docker
ERR=$?

if [ ${ERR} != 0 ] 
	then
echo "### Docker not installed on Ubuntu, will install it now"
sudo apt-get -y -q install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get -y -q=2 update
sudo apt-get -y -q=2 install docker-ce docker-ce-cli containerd.io
sudo apt-get -y -q=2 install docker
	else
	echo "### Docker is already installed"
fi

# TODO: check if docker has been installed properly


# optimize ubuntu OS to run the container
if [ ! -e ${DOCKFILE} ] 
	then
cat <<'EOF'|sudo tee -a ${DOCKFILE}
vm.max_map_count=262144
EOF
	sudo sysctl -p ${DOCKFILE}
fi


# TODO: check the optimizations required before implementing them 


# TODO: check if the elastic docker image exists first

# TODO: check if the image is running first
CONTID=$(sudo docker ps --filter ancestor=${IMAGE} --filter status=running --format "{{.ID}}")


if [ ! -z "$CONTID" ]  
	then
	echo "### Container id: ${CONTID}"
	# the image exists and it is running
	# report statistics about the runnng container
	sudo docker stats --no-stream --format "table {{.ID}}|{{.Name}}|{{.CPUPerc}}|{{.MemUsage}}|{{.MemPerc}}|{{.NetIO}}|{{.BlockIO}}|{{.PIDs}}" ${CONTID}

	else
	# run the container, pull it from docker hub if necessary and detach
	echo "### Downloading and running elastic search container"
	sudo docker run -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" ${IMAGE}
fi





